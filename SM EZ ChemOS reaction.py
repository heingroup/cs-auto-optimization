"""
Script for receiving and relaying instructions from ChemOS to a Chemspeed executing reactions and sending reaction
samples to an Agilent HPLC.

The script assumes that the next file which appears in the HPLC data output is associated with the next reaction which
does not have processed data.
"""
import time
import pathlib
import logging
from datetime import datetime
from aghplctools.hplc import HPLCTarget, datawatch
from aghplctools.ingestion import pull_hplc_area_from_txt
from chemos_interface.config import CONDITIONS_KEY, MEASUREMENTS_KEY
from chemos_interface.io import get_oldest_parameters, write_completed_parameters
from stock_solutions import *  # import solution concentrations

_log_dir = pathlib.Path(f'{pathlib.os.getcwd()}/logs')
if _log_dir.is_dir() is False:
    _log_dir.mkdir()
_logfile = pathlib.Path(f'{_log_dir}/{datetime.now().date()}.log')

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
    handlers=[  # log to stream and file
        logging.FileHandler(_logfile),
        logging.StreamHandler()
    ]
)
logger = logging.getLogger('CCC orchestration ')

# set this flag to True for crash recovery
CRASH_RECOVERY = False


def convert_chemos_parameters(dct):
    """
    Interprets the ChemOS parameters and converts them into the appropriate values for the ChemSpeed

    :param dict dct: dictionary of values received from ChemOS
    :return: updated dictionary
    """
    # check for all valid dictionary keys
    if chemos_parameters <= dct.keys() is False:
        raise KeyError(f'One or more keys are missing from the ChemOS parameters: '
                       f'{", ".join(val for val in chemos_parameters - dct.keys())}')

    # save original values
    dct['chemos_proposed_values'] = {
        key: dct[key] for key in chemos_parameters
    }

    # volume of Pd solution to dispense
    vol_Pd = (
        n_OTs  # number of moles of vinyl tosylate
        * dct['Pd_mol%']  # times catalyst loading
        / 100.  # which is given as a percent
        / catalyst_concentration  # divided by the catalyst concentration
    )
    vol_Pd.prefix = 'u'  # change prefix to uL
    vol_Pd.real = round(vol_Pd.real, 0)  # round to nearest uL
    dct['Pd_vol'] = float(vol_Pd)
    dct['Pd_mol%'] = (  # back calculate to effective mole percent
        vol_Pd
        * catalyst_concentration
        * 100.
        / n_OTs
    ).specific_prefix('')

    # volume of Ligand to dispense
    ligand_index = ligand_array.index(dct['P_ligand'])
    n_Pd = catalyst_concentration * vol_Pd  # number of moles of Pd
    n_ligand = n_Pd * dct['P/Pd_ratio']  # number of moles of ligand
    for ind, CAS in enumerate(LIGAND_ORDERING):
        if ind == ligand_index:
            ligand = ligand_array[ligand_index]  # get LigandStock instance
            vol = n_ligand / ligand.concentration
            vol.prefix = 'u'  # change to micro prefix
            vol.real = round(vol.real, 0)  # round value
            dct[f'{CAS}_vol'] = float(vol)  # store the rounded value
            dct['P/Pd_ratio'] = (  # back-calculate and store the actual ratio
                    vol
                    * ligand.concentration
                    / n_Pd
            ).specific_prefix('')
        else:  # otherwise volume is 0
            dct[f'{CAS}_vol'] = 0.

    # volume of boronic acid
    dct['ArBA_vol'] = float(vol_ArBA)

    # write constants
    dct['E-Tosyl_IS_vol'] = vol_OTs_IS.specific_prefix('u')
    ACN_vol = vol_organic - (
        vol_Pd
        + vol
        + vol_OTs_IS
        + vol_ArBA
    )
    if round(ACN_vol, 0) < 0:
        raise ValueError('A negative volume was encountered for solvent fill. An overfill has been encountered.')
    dct['ACN_vol'] = round(ACN_vol.specific_prefix('u'))
    dct['K3PO4_vol'] = vol_base.specific_prefix('u')
    return dct


def calculate_result():
    """
    Calculates the result of the reaction using results keys and target values

    :return: result value
    """
    results = {}
    for key in results_keys:
        for target in targets:
            if target.name == results_keys[key]['num']:
                num = target.areas[-1]
            elif target.name == results_keys[key]['denom']:
                denom = target.areas[-1]
        if denom == 0.:  # value error catch for 0 denominator
            value = 0.
        else:
            value = num / denom
            if 'RF' in results_keys[key]:  # if a response factor is in dictionary
                value *= results_keys[key]['RF']
        results[key] = value
    return results


def append_if_unlocked(file_path: str, line: str) -> bool:
    """
    Attempts to append the line to the provided file (in append mode). Returns True if successful and False if an error
    was encountered. Avoids errors resulting from the ChemSpeed and Python interacting with the file at the same time.

    :param file_path: file path to write to
    :param line: line to write
    :return: success of writing
    """
    try:
        with open(file_path, 'a') as f:
            f.write(line)
        return True
    except (IOError, PermissionError):  # catch file open errors
        return False
    except KeyboardInterrupt:  # correctly halt if interrupted
        logger.error('Keyboard interrupt when attempting file write')
        raise KeyboardInterrupt
    except Exception as e:
        logger.error(f'Unspecified exception: {e}')
        return False


def write_if_unlocked(file_path: str, text: str) -> bool:
    """
    Attempts to write the provided text to the file path (in write mode). Returns True if successful and False if an
    error was encountered. Avoids errors resulting from the ChemSpeed and Python interacting with the file at the
    same time.

    :param file_path: file path to write to
    :param text: text to write
    :return: success
    """
    try:
        with open(file_path, 'wt') as f:
            f.write(text)
        return True
    except (IOError, PermissionError):  # catch file open errors
        return False
    except KeyboardInterrupt:  # correctly halt if interrupted
        logger.error('Keyboard interrupt when attempting file write')
        raise KeyboardInterrupt
    except Exception as e:
        logger.error(f'Unspecified exception: {e}')
        return False


wl = {'wavelength': 210.}

# specify wavelengh/rt for all desired peaks
targets = [
    HPLCTarget(name='IS', retention_time=4.93, wiggle=0.08, **wl),
    HPLCTarget(name='ArBA', retention_time=4.76, wiggle=0.08, **wl),
    HPLCTarget(name='E-Tosyl', retention_time=6.13, wiggle=0.2, **wl),
    HPLCTarget(name='Z-PR', retention_time=7.01, wiggle=0.05, **wl),
    HPLCTarget(name='E-PR', retention_time=7.12, wiggle=0.05, **wl),
]

# change subdirectory of datawatch here
datawatch.subdirectory = None

# mark existing files as processed
processed = set(datawatch.contents)

logger.info(f'Writing reaction parameters and headers to ChemSpeed instruction file. ')
# chemspeed watch file
CHEMSPEED_CSV = pathlib.Path(
    f'C://Chemspeed/{datetime.now().date()} chemspeed values.csv'
)

# expected parameters from ChemOS
chemos_parameters = {
    'Pd_mol%',  # Pd loading (1-5%)
    'P/Pd_ratio',  # Pd:ligand ratio (1:0-3)
    'P_ligand',  # chosen ligand
    'Rxn_temp',  # reaction temperature
}

# header titles for parameters
parameter_headers = [
    'Rxn_temp',  # reaction temperature
    'Pd_vol',  # catalyst volume

    # ligands
    '554-70-1_vol',  # PEt3
    '6476-37-5_vol',  # PCy2Ph
    '1000171-05-0_vol',  # "1-Cy2P-2’,4’,6’-MeO-Ph2"
    '117672-33-0_vol',  # "PPh2(2,6-Me2-Ph)"
    '53111-20-9_vol',  # PPh2(2-OMe-Ph)
    '2741-38-0_vol',  # PallylPh2
    '224311-51-7_vol',  # JohnPhos
    '5074-71-5_vol',  # P(C6F5)2Ph
    '1262046-34-3_vol',  # RockPhos
    '739-58-2_vol',  # PhAtaPhos
    '657408-07-6_vol',  # SPhos
    '856407-37-9_vol',  # (R)SITCP
    '23897-15-6_vol',  # P(mesityl)3
    '121898-64-4_vol',  # "P(3,5-Me2-4-OMe-Ph)3"
    '1308652-66-5_vol',  # Et-PhenCarPhos
    '6163-58-2_vol',  # PoTol3
    '52090-23-0_vol',  # Ph2P(CH2)3Si(OEt)3
    '322647-83-6_vol',  # "PPh (4-(2,2-(CF3)2-F7pent)-Ph)2"
    '1620882-90-7_vol',  # "PtBu2(E-but-2-enyl), m-CroPhos"
    '932710-63-9_vol',  # AtaPhos
    '819867-24-8_vol',  # PhSPhos
    '4731-53-7_vol',  # P(nOct)3
    '2234-97-1_vol',  # PnPr3

    'E-Tosyl_IS_vol',  # tosyl/IS volume
    'ArBA_vol',  # boronic acid
    'ACN_vol',  # additional volume for acetonitrile
    'K3PO4_vol',  # volume of base
]

# defined response values (used to calculate the success of the reaction
results_keys = {
    'E-PR AY': {'num': 'E-PR', 'denom': 'IS', 'RF': 14.672},
    'Z-PR AY': {'num': 'Z-PR', 'denom': 'IS', 'RF': 13.489},
}

# if it doesn't exist, create it with header line
if not CHEMSPEED_CSV.is_file():
    with open(CHEMSPEED_CSV, 'wt') as f:
        f.write(
            f'Experiment_ID'  # experiment number
            f',{",".join(key for key in parameter_headers)}'  # parameter header names
            f',{",".join(f"{target.name}" for target in targets)}'  # target name columns
            f',{",".join(key for key in sorted(results_keys))}\n'  # results keys
        )

_RXN_DATA = 1  # counter for rxn number of data
_RXN_DEF_NUM = 1  # reaction number setter
_RXN_DICT = {}  # tracker for reaction number and data

while True:
    # check for new input file
    try:
        new_parameters = get_oldest_parameters(move_file=True)
    except PermissionError:  # quick catch for the permission error
        continue
    if new_parameters is not None:  # if a new file was found
        _RXN_DICT[_RXN_DEF_NUM] = new_parameters
        _RXN_DICT[_RXN_DEF_NUM][CONDITIONS_KEY] = convert_chemos_parameters(_RXN_DICT[_RXN_DEF_NUM][CONDITIONS_KEY])
        logger.info(
            f'Adding new parameters from "{_RXN_DICT[_RXN_DEF_NUM]["filename"]}" to CSV as reaction #{_RXN_DEF_NUM}'
        )
        # write data to csv
        while append_if_unlocked(
            CHEMSPEED_CSV,
            f'{_RXN_DEF_NUM},'
            f'{",".join(str(_RXN_DICT[_RXN_DEF_NUM][CONDITIONS_KEY][key]) for key in parameter_headers)}\n'
        ) is False:
            time.sleep(1)  # wait one second then try again
        _RXN_DEF_NUM += 1

    # look for new data and write to csv and pickle output
    elif len(datawatch.contents) > len(processed):
        # get the first new file
        newfile = list(set(datawatch.contents) - processed)[0]

        logger.info(f'Retrieving HPLC data for reaction #{_RXN_DATA}')
        # pull peak table from file
        report_dict = pull_hplc_area_from_txt(newfile)

        # retrieve lines and add data to appropriate line for convenient access to data
        with open(CHEMSPEED_CSV, 'r') as f:  # retrieve current lines
            lines = f.readlines()

        data = [target.add_from_pulled(report_dict)[0] for target in targets]
        results = calculate_result()

        for ind, line in enumerate(lines):  # modify appropriate line
            col_one = line.split(',')[0]
            if col_one.isnumeric() and int(col_one) == _RXN_DATA:
                lines[ind] = lines[ind].strip('\n')
                lines[ind] += f',{",".join(str(val) for val in data)}'
                lines[ind] += f',{",".join(str(results[key]) for key in sorted(results))}\n'

        # write modified data to file
        while write_if_unlocked(CHEMSPEED_CSV, f'{"".join([line for line in lines])}') is False:
            time.sleep(1.)

        # add new file name to set of processed files
        processed.add(newfile)

        # add results ratios
        results['Pd_mol%'] = _RXN_DICT[_RXN_DATA][CONDITIONS_KEY]['Pd_mol%']

        # add data to dictionary and write ChemOS pickle file
        _RXN_DICT[_RXN_DATA][MEASUREMENTS_KEY] = results
        write_completed_parameters(
            _RXN_DICT[_RXN_DATA],
            remove_conditions=True,
        )

        # increase data counter
        _RXN_DATA += 1

    else:
        time.sleep(5.)
