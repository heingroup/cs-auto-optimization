"""creates dummy files for testing the Python/ChemSpeed/ChemStation portion of the code (no ChemOS integration)"""

from chemos_interface.config import CONDITIONS_KEY
from chemos_interface.debug import write_dummy_parameter_file

reactions = {
    1: {
        'Pd_mol%': 1.,  # Pd loading (1-5%)
        'P/Pd_ratio': 0.5,  # Pd:ligand ratio (1:0-4)
        'P_ligand': '554-70-1',  # chosen ligand
        'Rxn_temp': 20,  # reaction temperature (10-40)
    },
    2: {
        'Pd_mol%': 2.,  # Pd loading (1-5%)
        'P/Pd_ratio': 1.,  # Pd:ligand ratio (1:0-4)
        'P_ligand': '2741-38-0',  # chosen ligand
        'Rxn_temp': 20,  # reaction temperature (10-40)
    },
    3: {
        'Pd_mol%': 2.5,  # Pd loading (1-5%)
        'P/Pd_ratio': 1.5,  # Pd:ligand ratio (1:0-4)
        'P_ligand': '2741-38-0',  # chosen ligand
        'Rxn_temp': 20,  # reaction temperature (10-40)
    },
    4: {
        'Pd_mol%': 3,  # Pd loading (1-5%)
        'P/Pd_ratio': 2.,  # Pd:ligand ratio (1:0-4)
        'P_ligand': '2741-38-0',  # chosen ligand
        'Rxn_temp': 20,  # reaction temperature (10-40)
    },
    5: {
        'Pd_mol%': 3.5,  # Pd loading (1-5%)
        'P/Pd_ratio': 2.5,  # Pd:ligand ratio (1:0-4)
        'P_ligand': '2741-38-0',  # chosen ligand
        'Rxn_temp': 20,  # reaction temperature (10-40)
    },
    6: {
        'Pd_mol%': 4.,  # Pd loading (1-5%)
        'P/Pd_ratio': 3.,  # Pd:ligand ratio (1:0-4)
        'P_ligand': '2741-38-0',  # chosen ligand
        'Rxn_temp': 20,  # reaction temperature (10-40)
    },
    7: {
        'Pd_mol%': 4.5,  # Pd loading (1-5%)
        'P/Pd_ratio': 3.5,  # Pd:ligand ratio (1:0-4)
        'P_ligand': '2741-38-0',  # chosen ligand
        'Rxn_temp': 20,  # reaction temperature (10-40)
    },
    8: {
        'Pd_mol%': 5.,  # Pd loading (1-5%)
        'P/Pd_ratio': 4.,  # Pd:ligand ratio (1:0-4)
        'P_ligand': '2741-38-0',  # chosen ligand
        'Rxn_temp': 20,  # reaction temperature (10-40)
    },
}

for rxn in reactions:
    write_dummy_parameter_file(
        {
            CONDITIONS_KEY: reactions[rxn],
        }
    )
