"""data helper classes for calculations and simplification of logic"""
from typing import Union, List
from unithandler.base import UnitFloat

DEFAULT_CONCENTRATION = 'M'


class LigandStock:
    def __init__(self,
                 CAS: str = None,
                 name: str = None,
                 concentration: Union[float, UnitFloat] = None,
                 ):
        """
        Helper class for managing ligand names, CAS #'s, and concentrations

        :param CAS: CAS number
        :param name: human-readable names
        :param concentration: concentration for the stock solution
        """
        self.CAS = CAS
        self.name = name
        self._conc = None
        self.concentration = concentration

    def __str__(self):
        return f'{self.name} ({self.CAS}) at {self.concentration}'

    def __repr__(self):
        return self.__str__()

    @property
    def concentration(self) -> UnitFloat:
        """concentration"""
        return self._conc

    @concentration.setter
    def concentration(self, value: Union[float, UnitFloat]):
        self._conc = UnitFloat(value)

    def __eq__(self, other: str):
        """checkes whether the other string is the name or CAS of the ligand"""
        return other == self.name or other == self.CAS


class LigandArray:
    def __init__(self):
        """Helper class for indexing an array of stock solutions to maintain order"""
        self.array: List[LigandStock] = []

    def __str__(self):
        return f'{self.__class__.__name__} with {len(self.array)} stock solutions'

    def __repr__(self):
        return self.__str__()

    def __getitem__(self, item: Union[int, str]) -> LigandStock:
        if type(item) is int:
            return self.array[item]
        elif type(item) is str:
            for ligand in self.array:
                if item == ligand:
                    return ligand
        raise AttributeError(f'The item {item} is not in the {self.__class__.__name__} instance')

    def add_ligand(self, ligand: LigandStock):
        """Adds the Ligand to the array (maintains order)"""
        self.array.append(ligand)

    def index(self, item: str):
        """indexes the item in the stock solution array"""
        return self.array.index(item)
