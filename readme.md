# Data-science driven autonomous process optimization

This code is that used in the [following publication](tbd). It was used to convey instructions from ChemOS to a 
Chemspeed SWING robot and interpret results from an Agilent HPLC. The setup file indicates the dependency versions 
used in the work.  

This code is copyright Lars Yunker // Hein Group 2020 and is distributed under the MIT open source license. 
