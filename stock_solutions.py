from unithandler.base import UnitFloat
from helpers import LigandStock, LigandArray

M = UnitFloat(1., 'mol/L')  # molar unit scalar
uL = UnitFloat(1., 'uL')  # microliter scalar

LIGAND_ORDERING = [
    '554-70-1',  # PEt3
    '6476-37-5',  # PCy2Ph
    '1000171-05-0',  # "1-Cy2P-2’,4’,6’-MeO-Ph2"
    '117672-33-0',  # "PPh2(2,6-Me2-Ph)"
    '53111-20-9',  # PPh2(2-OMe-Ph)
    '2741-38-0',  # PallylPh2
    '224311-51-7',  # JohnPhos
    '5074-71-5',  # P(C6F5)2Ph
    '1262046-34-3',  # RockPhos
    '739-58-2',  # PhAtaPhos
    '657408-07-6',  # SPhos
    '856407-37-9',  # (R)SITCP
    '23897-15-6',  # P(mesityl)3
    '121898-64-4',  # "P(3,5-Me2-4-OMe-Ph)3"
    '1308652-66-5',  # Et-PhenCarPhos
    '6163-58-2',  # PoTol3
    '52090-23-0',  # Ph2P(CH2)3Si(OEt)3
    '322647-83-6',  # "PPh (4-(2,2-(CF3)2-F7pent)-Ph)2"
    '1620882-90-7',  # "PtBu2(E-but-2-enyl), m-CroPhos"
    '932710-63-9',  # AtaPhos
    '819867-24-8',  # PhSPhos
    '4731-53-7',  # P(nOct)3
    '2234-97-1',  # PnPr3
]

stock_kwargs = {
    '554-70-1': 'PEt3',
    '6476-37-5': 'PCy2Ph',
    '1000171-05-0': '"1-Cy2P-2’,4’,6’-MeO-Ph2"',
    '117672-33-0': '"PPh2(2,6-Me2-Ph)"',
    '53111-20-9': 'PPh2(2-OMe-Ph)',
    '2741-38-0': 'PallylPh2',
    '224311-51-7': 'JohnPhos',
    '5074-71-5': 'P(C6F5)2Ph',
    '1262046-34-3': 'RockPhos',
    '739-58-2': 'PhAtaPhos',
    '657408-07-6': 'SPhos',
    '856407-37-9': '(R)SITCP ',
    '23897-15-6': 'P(mesityl)3',
    '121898-64-4': '"P(3,5-Me2-4-OMe-Ph)3"',
    '1308652-66-5': 'Et-PhenCarPhos',
    '6163-58-2': 'PoTol3',
    '52090-23-0': 'Ph2P(CH2)3Si(OEt)3',
    '322647-83-6': '"PPh (4-(2,2-(CF3)2-F7pent)-Ph)2"',
    '1620882-90-7': '"PtBu2(E-but-2-enyl), m-CroPhos"',
    '932710-63-9': 'AtaPhos',
    '819867-24-8': 'PhSPhos',
    '4731-53-7': 'P(nOct)3',
    '2234-97-1': 'PnPr3',
}

# base_ligand_concentration = 0.04 * M
ligand_array = LigandArray()
for cas in LIGAND_ORDERING:
    ligand_array.add_ligand(
        LigandStock(
            CAS=cas,
            name=stock_kwargs[cas],
            concentration=0.02 * M,
        )
    )


catalyst_concentration = 0.01 * M

vinyl_tosylate_concentration = 0.5 * M

boronic_acid_concentration = 0.5 * M


############# reaction constants

vol_OTs_IS = 20 * uL  # vinyl tosylate and internal standard volume

vol_base = 60 * uL  # volume of base

# number of moles of vinyl tosylate
n_OTs = vinyl_tosylate_concentration * vol_OTs_IS

# boronic acid volume
boronic_acid_equivalents = 1.5
vol_ArBA = (
    n_OTs  # number of mols vinyl tosylate
    * boronic_acid_equivalents  # times equivalents
    / boronic_acid_concentration
)
vol_ArBA.prefix = 'u'

# volume for solvent # 1
vol_organic = 200 * uL

# volume for solvent # 2
vol_aqueous = 60 * uL


test_values = {
    'Pd_loading': 5.,  # Pd loading (1-5%)
    'ligand_ratio': 2.,  # Pd:ligand ratio (1:0-3)
    'ligand': 'P(Ph)3',  # chosen ligand
    'Rxn_temp': 20,  # reaction temperature
    # 'BA_ratio': 1.1,  # OTs:Boronic acid ratio (1:1-1.5)
}

