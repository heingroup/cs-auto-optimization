from setuptools import setup

setup(
    name='merck-cs',
    version='2.1',
    url='https://gitlab.com/heingroup/merck-cs',
    license='MIT',
    author='Lars Yunker // Hein Group',
    author_email='larsy@chem.ubc.ca',
    description='Scripts used to direct a ChemSpeed for reaction optimization',
    python_requires='>=3.6',
    install_requires=[
        'unithandler>=1.3.3',
        'hein_utilities==0.1.6',
        'aghplctools==0.2.0',
        'git+https://gitlab.com/heingroup/chemos_interface.git@v0.1.2#egg=chemos_interface',
    ],
)
